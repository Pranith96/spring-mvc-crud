package com.demo.springmvc.dao;

import java.util.List;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.demo.springmvc.model.Student;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<Student> findAllStudent() {
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		Query query = session.createQuery("from Student");
		List<Student> students = query.list();
		t.commit();
		session.close();
		return students;
	}

	@Override
	public Optional<Student> findById(int id) {
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		Query query = session.createQuery("from Student where id = :id");
		query.setParameter("id", id);
		Optional<Student> students = query.uniqueResultOptional();
		t.commit();
		session.close();
		return students;
	}

	@Override
	public void updateStudent(Student student) {
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		session.update(student);
		t.commit();
		session.close();
	}

	@Override
	public void deleteStudentRecord(int id) {
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		Student student = session.load(Student.class, new Integer(id));
		if (student != null) {
			session.delete(student);
		}
		t.commit();
		session.close();

	}

	@Override
	public int updateStudentName(String name, int id) {
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		Query query = session.createQuery("update Student set name=:name where id = :id");
		query.setParameter("name", name);
		query.setParameter("id", id);
		int i = query.executeUpdate();
		t.commit();
		session.close();
		return i;

	}

	@Override
	public void saveData(Student student) {
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		session.saveOrUpdate(student);
		t.commit();
		session.close();
	}
}
