package com.demo.springmvc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.demo.springmvc.model.Student;

@Repository
public class StudentDaoImpl implements StudentDao {

	@Autowired
	JdbcTemplate template;

	@Override
	public Student saveData(Student student) {

		String insert = "Insert into student_data  values('"+student.getId()+"','" + student.getName() + "','"
				+ student.getMobileNumber() + "','" + student.getPassword() + "');";
		template.update(insert);
		String select = "Select * from student_data where id ='" + student.getId() + "';";
		return template.query(select, new ResultSetExtractor<Student>() {

			@Override
			public Student extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					Student student = new Student();
					student.setId(rs.getInt("id"));
					student.setName(rs.getString("name"));
					student.setMobileNumber(rs.getString("mobile_number"));
					student.setPassword(rs.getString("password"));
					return student;
				}
				return null;
			}
		});

	}

	@Override
	public List<Student> getAllStudentData() {
		String selectQuery = "Select * from student_data";
		List<Student> data = template.query(selectQuery, new StudentMapper());
		return data;
	}

	@Override
	public Student getStudentDataById(int id) {
		String select = "Select * from student_data where id = '" + id + "';";
		Student response = template.query(select, new ResultSetExtractor<Student>() {

			@Override
			public Student extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					Student student = new Student();
					student.setId(rs.getInt("id"));
					student.setName(rs.getString("name"));
					student.setMobileNumber(rs.getString("mobile_number"));
					student.setPassword(rs.getString("password"));
					return student;
				}
				return null;
			}

		});
		return response;
	}

	@Override
	public int updateStudent(Student student) {
		String updateQuery = "update student_data set name = '" + student.getName() + "', mobile_number= '"
				+ student.getMobileNumber() + "'" + ", password = '" + student.getPassword() + "' where id = '"
				+ student.getId() + "'";
		int i = template.update(updateQuery);
		return i;
	}

	@Override
	public int deleteStudentRecord(int id) {
		String deleteQuery = "delete from student_data where id='" + id + "'";
		int i = template.update(deleteQuery);
		return i;
	}

	@Override
	public int updateStudentName(String name, int id) {
		String updateName = "update student_data set name = '" + name + "' where id = '"+id+"'";
		int i = template.update(updateName);
		return i;
	}

}
