package com.demo.springmvc.dao;

import java.util.List;

import com.demo.springmvc.model.Student;

public interface StudentDao {

	Student saveData(Student student);

	List<Student> getAllStudentData();

	Student getStudentDataById(int id);

	int updateStudent(Student student);

	int deleteStudentRecord(int id);

	int updateStudentName(String name, int id);

}
