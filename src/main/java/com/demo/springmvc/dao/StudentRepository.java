
package com.demo.springmvc.dao;

import java.util.List;
import java.util.Optional;

import com.demo.springmvc.model.Student;

public interface StudentRepository {

	List<Student> findAllStudent();

	Optional<Student> findById(int id);

	void updateStudent(Student student);

	void deleteStudentRecord(int id);

	int updateStudentName(String name, int id);

	void saveData(Student student);
}
