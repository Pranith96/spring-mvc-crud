package com.demo.springmvc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.demo.springmvc.model.Student;

public class StudentMapper implements RowMapper<Student> {

	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {

		Student student = new Student();
		student.setId(rs.getInt("id"));
		student.setPassword(rs.getString("password"));
		student.setMobileNumber(rs.getString("mobile_number"));
		student.setName(rs.getString("name"));
		return student;
	}

}
