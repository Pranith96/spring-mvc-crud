package com.demo.springmvc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.demo.springmvc.dao.StudentDao;
import com.demo.springmvc.dao.StudentRepository;
import com.demo.springmvc.exceptions.StudentNotFoundException;
//import com.demo.springmvc.dao.StudentRepository;
import com.demo.springmvc.model.Student;

@Service
public class StudentRegistrationServiceImpl implements StudentRegistrationService {

	/*
	 * @Autowired StudentDao dao;
	 */
	@Autowired
	StudentRepository repository;

	@Override
	public String saveData(Student student) throws Exception {
		// Student response = dao.saveData(student);

		repository.saveData(student);
		/*
		 * ApplicationContext context = new
		 * AnnotationConfigApplicationContext(Student.class); Student student1 =
		 * (Student) context.getBean("student"); student1.setId(student.getId());
		 * student1.setName(student.getName());
		 * student1.setMobileNumber(student.getMobileNumber());
		 * student1.setPassword(student.getPassword()); System.out.println(student1);
		 */

		return "Data saved successfully";
	}

	@Override
	public List<Student> getAllData() throws StudentNotFoundException {
		// List<Student> response = dao.getAllStudentData();
		List<Student> response = repository.findAllStudent();
		if (response.isEmpty() || response == null) {
			throw new StudentNotFoundException("student details empty");
		}
		return response;
	}

	@Override
	public Student getStudentDataById(int id) throws StudentNotFoundException {
		// Student response = dao.getStudentDataById(id);
		Optional<Student> response = repository.findById(id);

		if (!response.isPresent()) {
			throw new StudentNotFoundException("Data id not found");
		}
		return response.get();
	}

	@Override
	public String updateStudentData(Student student) throws Exception {

		/*
		 * int data = dao.updateStudent(student); if (data < 0 || data == 0) { throw new
		 * Exception("Data is not updated"); }
		 */

		repository.updateStudent(student);
		return "Data updated Sucessfully";
	}

	@Override
	public String deleteStudentRecord(int id) throws Exception {

		/*
		 * int data = dao.deleteStudentRecord(id); if (data < 0 || data == 0) { throw
		 * new Exception("Data Is not deleted"); }
		 */
		repository.deleteStudentRecord(id);

		return "Data is deleted successfully";
	}

	@Override
	public String updateStudentName(String name, int id) throws Exception {

		/*
		 * int data = dao.updateStudentName(name, id); if (data < 0 || data == 0) {
		 * throw new Exception("Data is not updated"); }
		 */

		int data = repository.updateStudentName(name, id);
		if (data < 0 || data == 0) {
			throw new Exception("Data is not updated");
		}

		return "Data is updated Successfully";
	}

}
