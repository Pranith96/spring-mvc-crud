package com.demo.springmvc.service;

import java.util.List;

import com.demo.springmvc.exceptions.StudentNotFoundException;
import com.demo.springmvc.model.Student;

public interface StudentRegistrationService {

	String saveData(Student student) throws Exception;

	List<Student> getAllData() throws StudentNotFoundException;

	Student getStudentDataById(int id) throws StudentNotFoundException;

	String updateStudentData(Student student) throws Exception;

	String deleteStudentRecord(int id) throws Exception;

	String updateStudentName(String name, int id) throws Exception;

}
