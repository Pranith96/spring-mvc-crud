package com.demo.springmvc.controller;

import java.util.List;

import javax.validation.Constraint;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.springmvc.model.Student;
import com.demo.springmvc.service.StudentRegistrationService;

//@RestController
@Controller
@RequestMapping(value = "/api")
public class StudentRegistrationController {

	@Autowired
	StudentRegistrationService service;

	// @PostMapping("/save")
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveData(@RequestBody @Valid Student student) throws Exception {
		String detail = service.saveData(student);
		return detail;
	}

	// @GetMapping("/all")
	
	@ResponseBody
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<Student> getAllData() throws Exception {
		List<Student> detail = service.getAllData();
		return detail;
	}

	// @GetMapping("/student/{id}")
	@ResponseBody
	@RequestMapping(value = "/student/{id}", method = RequestMethod.GET)
	public ResponseEntity<Student> getStudentDataById(@PathVariable("id") int id) throws Exception {
		Student detail = service.getStudentDataById(id);
		return ResponseEntity.status(HttpStatus.OK).body(detail);
	}

	// http://localhost:8080/api/student/Mobile/iphone
	// http://localhost:8080/api/student?id=1
	// @GetMapping("/student/id")
	@ResponseBody
	@RequestMapping(value = "/student/id", method = RequestMethod.GET)
	public Student getStudentData(@RequestParam("id") int id) throws Exception {
		Student detail = service.getStudentDataById(id);
		return detail;
	}

	// @PutMapping("/student/update")
	@ResponseBody
	@RequestMapping(value = "/student/update", method = RequestMethod.PUT)
	public String updateStudentData(@RequestBody Student student) throws Exception {
		String detail = service.updateStudentData(student);
		return detail;
	}

	// @DeleteMapping("/student/delete/{id}")
	@ResponseBody
	@RequestMapping(value = "/student/delete/{id}", method = RequestMethod.DELETE)
	public String deleteStudentRecord(@PathVariable("id") int id) throws Exception {
		String detail = service.deleteStudentRecord(id);
		return detail;
	}

	// @PutMapping("/student/update/name")
	@ResponseBody
	@RequestMapping(value = "/student/update/name", method = RequestMethod.PUT)
	public String updateStudentName(@RequestParam("name") String name, @RequestParam("id") int id) throws Exception {
		String detail = service.updateStudentName(name, id);
		return detail;
	}
}
