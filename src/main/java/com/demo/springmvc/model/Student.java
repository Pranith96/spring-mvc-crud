package com.demo.springmvc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "student_table")
public class Student {

	@Id
	@Column(name = "id")
	private Integer id;
	@Column(name = "student_name",nullable = false, length = 25)
	private String name;
	@Column(name = "student_mobilenumber")
	private String mobileNumber;
	@Column(name = "password",nullable = false)
	private String password;

	public Student() {
	}

	public Student(Integer id, String name, String mobileNumber, String password) {
		this.id = id;
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", mobileNumber=" + mobileNumber + ", password=" + password
				+ "]";
	}
}
