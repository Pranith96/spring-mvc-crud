package com.demo.springmvc.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Student Details Not Exists")
public class StudentNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private String message;

	public StudentNotFoundException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
